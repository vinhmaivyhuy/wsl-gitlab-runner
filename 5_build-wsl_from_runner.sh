#! /bin/bash

set -e

# Update the package manager and upgrade the system
# #################################################
apt-get -y update
apt-get -y upgrade
apt-get -y install openssh-client
apt-get -y install net-tools passwd bzip2 sudo vim curl 
#apt-get -y install locales locales-all 
ssh-keygen -A

# Set the locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

# Setup Gitlab
# #################################################
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt-get -y install gitlab-runner

cp ./gitlab.wsl/wsl.conf /etc
cp ./gitlab.wsl/dot-bashrc /home/gitlab-runner/.bashrc
cp ./gitlab.wsl/dot-profile /home/gitlab-runner/.profile
mkdir -p /home/gitlab-runner/bin
cp ./gitlab.wsl/start /home/gitlab-runner/bin
cp ./gitlab.wsl/stop /home/gitlab-runner/bin
cp ./gitlab.wsl/status /home/gitlab-runner/bin
cp ./gitlab.wsl/register /home/gitlab-runner/bin
chown -R gitlab-runner:gitlab-runner /home/gitlab-runner
chmod u+x /home/gitlab-runner/bin/s*
echo "gitlab-runner  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/gitlab-runner
cp ./gitlab.wsl/gitlab-runner /etc/init.d/
#
echo usermod -a -G docker gitlab-runner
usermod -a -G docker gitlab-runner
su - gitlab-runner bash -c "ssh-keygen -q -t rsa -b 4096"

