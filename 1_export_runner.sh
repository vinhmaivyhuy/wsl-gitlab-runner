#!/bin/bash
set -e

echo Docker process need to be started
docker pull ubuntu
docker container prune --force
docker run -t ubuntu date
dockerContainerID=$(docker container ls -a | grep -i ubuntu | awk '{print $1}')
docker export $dockerContainerID > /mnt/c/temp/ubuntu.tar
